var DailyMessage = require('../models/DailyMessage');

/**
 * GET /message
 * Daily message form page.
 */
exports.getDailyMessage = function(req, res) {
    res.render('message', {
        title: 'Message'
    });
};

/**
 * POST /message
 * Create daily message.
 */
exports.postDailyMessage = function(req, res) {
    req.assert('startDate', 'Podaj datę rozpoczęcia wyświetlania komuniaktu').notEmpty();
    req.assert('endDate', 'Podaj datę zakończenia wyświetlania komuniaktu').notEmpty();
    req.assert('message', 'Podaj treść komuniaktu').notEmpty();

    var stDate = new Date(req.body.startDate);
    var enDate = new Date(req.body.endDate).setHours(24);
    var errors = req.validationErrors();

    if (errors) {
        req.flash('errors', errors);
        return res.redirect('/message');
    }

    var message = new DailyMessage({
        author: req.user.email,
        content: req.body.message,
        startDate: stDate,
        endDate: enDate
    });

    message.save(function (err) {
        if (err) {
            return next(err);
        }
        req.flash('success', { msg: 'Dodano komunikat dnia' });
        res.redirect('/');
    });
};