var News = require('../models/News');

/**
 * GET /news
 * News form page.
 */
exports.getNews = function(req, res) {
    res.render('news', {
        title: 'News'
    });
};

/**
 * POST /news
 * Create news.
 */
exports.postNews = function(req, res) {
    req.assert('news', 'Podaj treść').notEmpty();

    var newsDate = new Date();

    var errors = req.validationErrors();

    if (errors) {
        req.flash('errors', errors);
        return res.redirect('/news');
    }

    var news = new News({
        author: req.user.name,
        content: req.body.news,
        newsDate: newsDate
    });

    news.save(function (err) {
        if (err) {
            return next(err);
        }
        req.flash('success', { msg: 'Dodano wiadomość' });
        res.redirect('/');
    });
};