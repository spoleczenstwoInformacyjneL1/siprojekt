var DailyMessage = require('../models/DailyMessage');
var News = require('../models/News');

/**
 * GET /
 * Home page.
 */
exports.index = function(req, res) {
  var now = new Date();
  DailyMessage
      .find({
        'startDate': {'$lte': now},
        'endDate': {'$gte': now}
      })
      .select('content')
      .exec(function (err, messages) {
        if (err) {
            res.render('home', {
                title: 'Home'
            })
        }
        else {
            News
                .find()
                .select('content')
                .exec(function (err, news) {
                    res.render('home', {
                        title: 'Home',
                        dailyMessages: messages,
                        news: news
                    });
                })
        }
    });
};