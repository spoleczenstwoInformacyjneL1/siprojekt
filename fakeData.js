var faker = require('faker');
var Osoba = require('./models/Osoba');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/test');
var db = mongoose.connection;



db.on('error', function (err) {
    console.error(err);
});

db.once('open', function () {
    console.log('open');
    fakerImport();
});

faker.locale = 'pl';

function fakerImport() {
    console.log('query');
    var savedCount = 0;
    var pendingCount = 0;
    console.log('removing Osobas');
    Osoba.remove({}, function (err) {
        if (err) {
            throw err;
        }
        var dbSize = 10000;
        for (var i = 0; i < dbSize; i++) {
            var osoba = new Osoba({
                ID: i,
                STI: (i%3===0?102:1),
                PLEC: (i%2===0?'M':'K'),
                PSL: null,
                NIP: null,
                NZW: faker.name.lastName(),
                PIM: faker.name.firstName((i%2)),
                OIM: faker.name.firstName(0),
                DIM: faker.name.firstName(i%2),
                OBL: null,
                DOS: null,
                DTW: faker.date.past(),
                DTU: faker.date.past(),
                OSOU: null,
                OSOW: null,
                DOWW: null,
                DOWU: (i%5===0?15244:null),
                MIM: faker.name.firstName(1),
                IDDOK: null,
                MEMO: null,
                RADR: null,
                NRL: null,
                UID: null,
                ADR1: null,
                ADR2: null,
                ADR3: null,
                NSTI: null,
                IDIIP: null,
                PNAZW: null,
                NZW2: null
            });
            osoba.save(function (err, obj) {
                if (err) {
                    console.error(err);
                }
                savedCount++;
                if (!(savedCount % 100)) {
                    console.log(savedCount + "//" + dbSize)
                }
                if (savedCount === dbSize) {
                    mongoose.disconnect();
                    console.log('Bum');
                }
            });
        }
    });
}