
require.config({
    baseUrl: "/js",
    paths  : {
        jquery      : "lib/jquery.min",
        underscore  : "lib/underscore-min",
        backbone    : "lib/backbone-min",
        bootstrap   : "lib/bootstrap.min",
        Main        : "app"
    }
});

// ładowanie zależności dla CSS i JavaScript
require( ["jquery"], ($) => {
    require( ["underscore"], (_) => {
        require( ["backbone"], (Backbone) => {
        	require( ["bootstrap"], (Backbone) => {
	            require( ["app"], (App) => {
	                window.console.log( "Załadowano AppJS..." );
	                let app = new App.default();
	                app.start();
	                window.app = app;
	            } );
	            window.console.log( "Załadowano bibliotekę Bootstrap..." );
	        } );
            window.console.log( "Załadowano Backbone, wersja: " + Backbone.VERSION );
        } );
        window.console.log( "Załadowano Underscore, wersja: " + _.VERSION );
    });
    window.console.log( "Załadowano jQuery, wersja: " + $.fn.jquery );
} );
