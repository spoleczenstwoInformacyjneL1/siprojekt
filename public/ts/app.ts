import {UserMenuView} from "./views/usermenu";

/**
 * Klasa główna aplikacji.
 * Uruchamia wszystkie widoki i modele.
 */
export default class Bootstrap
{
	/**
	 * Funkcja startowa aplikacji.
	 */
	public start()
	{
		Backbone.history.start();
		new UserMenuView( {el: "body"} );
	}
}
