import Backbone = require("backbone");

/**
 * Główny widok na stronie.
 * Pozwala na pojawianie się i chowanie menu.
 * Na razie posiada tylko taką funkcję.
 */
export class UserMenuView extends Backbone.View<Backbone.Model>
{
	/**
	 * Nazwa głównego selektora.
	 * @type {string}
	 */
	public el: string;

	/**
	 * Element menu zwracany po użyciu selektora JQuery.
	 * @type {JQuery}
	 */
	public $dropdown: JQuery;

	/**
	 * Konstruktor widoku.
	 * 
	 * @param {Backbone.ViewOptions<Backbone.Model>} options Opcje widoku.
	 */
	public constructor( options: Backbone.ViewOptions<Backbone.Model> )
	{
		// upewnij się że właściwość "el" jest przekazywana
		options = _.extend({
			el: "body"
		}, options);

		// konstruktor rodzica
		super( options );

		// przypisz funkcje initializującą
		this.initialize = this.initialize;
	}

	/**
	 * Rejestracja zdarzeń.
	 * 
	 * @return {any} Zdarzenia do zarejestrowania.
	 */
	public events(): any
	{
		return {
			"click .dropdown-toggle": "toggleMenu",
			"click": "hideMenu"
		};
	};

	/**
	 * Inicjalizacja danych.
	 */
	public initialize(): void
	{
		this.$dropdown = this.$el.find(".dropdown-menu");
	}

	/**
	 * Funkcja renderowania.
	 * 
	 * @return {UserMenuView} Widok.
	 */
	public render(): UserMenuView
	{
		return this;
	}

	/**
	 * Przełącza menu po kliknięciu w przycisk.
	 * 
	 * @param {JQueryMouseEventObject} event Zdarzenie JQuery.
	 */
	public toggleMenu( event: JQueryMouseEventObject )
	{
		if( this.$dropdown.css("display") === "none" || _.isUndefined(this.$dropdown.css("display")))
			this.$dropdown.css("display", "block");
		else
			this.$dropdown.css("display", "none");

		event.stopPropagation();
		return true;
	}

	/**
	 * Ukrywa menu po kliknięciu w cokolwiek na stronie.
	 */
	public hideMenu()
	{
		if( this.$dropdown.css("display") !== "none" )
			this.$dropdown.css( "display", "none" );
	}
}
