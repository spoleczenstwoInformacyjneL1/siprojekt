var mongoose = require('mongoose');

var dailyMessageSchema = new mongoose.Schema({
    author: String,
    content: String,
    startDate: Date,
    endDate: Date
});

var DailyMessage = mongoose.model('DailyMessage', dailyMessageSchema);

module.exports = DailyMessage;