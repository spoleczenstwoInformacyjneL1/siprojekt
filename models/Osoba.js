var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var osobaSchema = Schema({
    ID: Number,
    STI: Number,
    PLEC: String,
    PSL: String,
    NIP: String,
    NZW: String,
    PIM: String,
    OIM: String,
    DIM: String,
    OBL: String,
    DOS: String,
    DTW: Date,
    DTU: Date,
    OSOU: String,
    OSOW: String,
    DOWW: String,
    DOWU: Number,
    MIM: String,
    IDDOK: String,
    MEMO: String,
    RADR: Number,
    NRL: String,
    UID: Number,
    ADR1: String,
    ADR2: String,
    ADR3: String,
    NSTI: Number,
    IDIIP: String,
    PNAZW: String,
    NZW2: String
});

var Osoba = mongoose.model('Osoba', osobaSchema);

module.exports = Osoba;