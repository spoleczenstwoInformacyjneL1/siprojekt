var mongoose = require('mongoose');

var newsSchema = new mongoose.Schema({
    author: String,
    content: String,
    newsDate: Date
});

var News = mongoose.model('News', newsSchema);

module.exports = News;